
$(window).ready(function () {
	SetTextForAdaptive();	
});

$(window).resize(function () {
	SetTextForAdaptive();	
});

function SetTextForAdaptive(){
	width = window.innerWidth
      || document.documentElement.clientWidth
      || document.body.clientWidth;
	//Для высокого разрешения экрана.
	$('#region-text').text('Ваш регион: ');
	$('#account-link-text').text('Мой аккаунт');
	$('#cart-link-text').text('Корзина пуста');
	$('#map').attr('src','images/russia-map.png');
	$('#social-message > img').attr('src','images/social/message-blue.svg');
	$('#social-telegram > img').attr('src','images/social/telegram-blue.svg');
	$('#social-viber > img').attr('src','images/social/viber-blue.svg');
	$('#social-vk > img').attr('src','images/social/vk-blue.svg');
	$('#social-whatsapp > img').attr('src','images/social/whatsapp-blue.svg');
	$('#social-close > img').attr('src','images/social/close-blue.svg');
	$('#social-up > img').attr('src','images/social/up-blue.svg');
	$('#logo2').attr('src','images/logo2.png');
	$('#sidemenu > span').text('Полный каталог');

	if (width <= 1280) {
	    $('#region-text').text('г. ');	    
	    $('#account-link-text').text('Аккаунт');
		$('#cart-link-text').text('Корзина');
		$('#map').attr('src','images/russia-map773.png');
		$('#social-message > img').attr('src','images/social/message.svg');
		$('#social-telegram > img').attr('src','images/social/telegram.svg');
		$('#social-viber > img').attr('src','images/social/viber.svg');
		$('#social-vk > img').attr('src','images/social/vk.svg');		
		$('#social-whatsapp > img').attr('src','images/social/whatsapp.svg');
		$('#social-close > img').attr('src','images/social/close.svg');
		$('#social-up > img').attr('src','images/social/up.svg');
		$('#logo2').attr('src','images/logo2.png');
		$('#sidemenu > span').text('Полный каталог');
	}	
	if (width <= 1024) {
	    $('#region-text').text('г. ');	    
	    $('#account-link-text').text('');
		$('#cart-link-text').text('');
		$('#map').attr('src','images/russia-map570.png');
		$('#social-message > img').attr('src','images/social/message.svg');
		$('#social-telegram > img').attr('src','images/social/telegram.svg');
		$('#social-viber > img').attr('src','images/social/viber.svg');
		$('#social-vk > img').attr('src','images/social/vk.svg');		
		$('#social-whatsapp > img').attr('src','images/social/whatsapp.svg');
		$('#social-close > img').attr('src','images/social/close.svg');
		$('#social-up > img').attr('src','images/social/up.svg');
		$('#logo2').attr('src','images/logo2-229x38.png');
		$('#sidemenu > span').text('Каталог товаров');
	}	
}